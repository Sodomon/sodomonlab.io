---
title: "Agenda personal en lua gtk y sqlite3"
date: 2020-04-29T01:03:00-04:00
lastmod: 2020-04-29T01:03:00-04:00
draft: false
tags: ["lua", "lgi","sqlite","gtk"]
categories: ["Lua","GTK"]

toc: false
---

# Agenda personal
Agenda personal para registro de telefonos hecha en lua usando la libreria lgi para 
la intefaz en GTK, y usando sqlite como base de datos

- [lua](https://www.lua.org/)
- [Descargar](https://github.com/sodomon2/agenda_personal/archive/v1.0.tar.gz)
- [Repositorio en github ](https://github.com/sodomon2/agenda_personal/)

## Ejecucion
Para ejecutar el la agenda es simple,solo tienes que seguir 4 pasos

- descargar lua-lgi,sqlite3 y libnotify
- descargar lua
- ejecutar `make db`
- ejecutar agenda.lua

# Release v1.0

- [x] keyval integrado (entrar al presionar la tecla return)
- [x] añadido modulo de registro de usuario
- [x] añadido el makefile
- [x] añadido el submodulo de lgi

## Todo
- [x] keyval hablitilado en modulo de registro de usuarios
- [x] dialogo de ayuda
- [x] tray icon
- [x] notificaciones
- [ ] boton limpiar implementado
- [ ] buscador de contactos implementado
- [ ] añadido el submodulo de sqlite3 
- [x] añadido el submodulo de libnotify

## Enlaces 

- [Lua-LGI](https://github.com/pavouk/lgi)
- [Lua](https://github.com/lua/lua)
- [Sqlite3](https://www.sqlite.org/download.html)
- [LibNotify](https://github.com/GNOME/libnotify)
