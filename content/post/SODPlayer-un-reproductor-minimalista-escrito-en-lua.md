---
title: "SODPlayer Un Reproductor Minimalista"
date: 2020-09-19T23:11:23-04:00
lastmod: 2020-10-13T23:11:23-04:00
draft: false
tags: ["gtk","lgi","lua","Reproductor","gst"]
categories: ["Gstreamer","Player","GTK"]

toc: false
---

# SODPlayer

SODPlayer es un reproductor minimalista escrito en lua,usando el framework `GStreamer`
y GTK para la interfaz de usuario

`SODPlayer esta basado en : smplayer(GUI) y zplayer`

## KeyBindings
Key    | Descripcion
-------|-----------------------------------------
`Control+O`:| Para abrir el selector de video/musica
`Control+U`:| Para abrir un video/musica desde internet
`Control+P`:| Para abrir las preferencias
`Control+Q`:| Para salir de SODPlayer
`Space`:| Pause/Play
`Q/q`:| Salir de SODPlayer
`Right`:| Adelanta el video
`Left`:| Atrasa el video
`F/f/F11`:| Pantalla completa
`Escape`:| Modo Ventana
`M/m`:| Mute
`h`:| Muestra los controles [WIP]


## Instalacion

```
git clone https://github.com/sodomon2/SODPlayer.git
cd SODPlayer/
[sudo] make install
```

## Dependencias 

- [Gstreamer](https://gstreamer.freedesktop.org/download/)
- [lua-lgi](https://github.com/pavouk/lgi)
- [lua](https://github.com/lua/lua)

## Quieres Contribuir?
si quieres contribuir no dudes en forkear el [repositorio](https://github.com/sodomon2/SODPlayer) y mandar un Pull request


#### Enlaces 

- [github](https://github.com/sodomon2/SODPlayer)
