---
title: "Plasma-desktop fix video intel"
date: 2023-11-22T06:30:16-04:00
lastmod: 2023-11-22T06:32:34-04:00
draft: false
tags: ["alpine","guia"]
categories: ["Alpine Linux","Guia"]

toc: false
---

La intención de esto es intentar arreglar los problemas causados por el módulo xf86-video-intel, en mi caso he arreglado el problema de la pantalla negra al iniciar sesión y también he arreglado algúnos segfault causado por el video.

# Pasos:

- 1: Crear directorio `/usr/share/X11/xorg.conf.d` (alpine) o `/etc/X11/xorg.conf.d` (todos los demás sistemas)

- 2: Crear el archivo `10-monitos.conf` he insertar

```bash
Section "Device"
    Identifier "Device0"
    Driver "intel"
    Option "DRI"  "iris"
EndSection
```
- 3: Reiniciar el sistema

## Notas

- `iris` es el controlador DDX de Intel para GPUs recientes.
- En mi caso funcionó con `Intel HD Graphics 530`.

### Esto se ha escrito en:

![imagen](https://user-images.githubusercontent.com/49420637/284816888-4f510b16-90d4-40a0-92e5-83100c5684a3.png)

