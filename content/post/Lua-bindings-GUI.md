---
title: "Herramientas de interfaz gráfica de usuario para lua"
date: 2021-01-23T15:27:00-04:00
lastmod: 2021-01-23T15:27:00-04:00
draft: false
tags: ["gui","lua"]
categories: ["Lua","GUI","Bindings"]

toc: false
---
En este articulo mostraremos las librerias para crear [Interfaces graficas](https://es.wikipedia.org/wiki/Interfaz_gr%C3%A1fica_de_usuario) con [lua](lua.org)

# Awtk Bindings
- https://github.com/zlgopen/awtk-lua

# Curses Bindings
- https://github.com/lcurses/lcurses

# FLTK Bindings
- https://github.com/stetre/moonfltk      
- https://github.com/siffiejoe/lua-fltk4lua       
- http://lua-fltk.sourceforge.net/        
- http://www.murga-projects.com/murgaLua/index.html

# FOX Bindings
- http://luaforge.net/projects/fxlua/

# Gtk bindings
- https://github.com/pavouk/lgi       
- https://bitbucket.org/lucashnegri/lgob      
- https://sourceforge.net/projects/lua-gnome/

# GLFW Bindings
- https://github.com/stetre/moonglfw

# GLUT Bindings
- https://github.com/stetre/moonglut

# ImGui Bindings
- https://github.com/patrickriordan/imgui_lua_bindings
- https://github.com/thenumbernine/lua-ffi-bindings
- https://github.com/sonoro1234/LuaJIT-ImGui

# JUCE Bindings
- https://github.com/peersuasive/luce/

# Motif Bindings
- https://github.com/mbalmer/luamotif

# Nuklear Bindings
- https://github.com/stetre/moonnuklear

# Qt4 Bindings
- https://www.nongnu.org/libqtlua/    
- https://github.com/mkottman/lqt     
- https://github.com/torch/qtlua

# Qt5 Bindings
- https://github.com/lqt5/lqt       
- https://www.nongnu.org/libqtlua/      
- https://github.com/TerraME/libqtlua

# Tk/Tcl Bindings
- https://github.com/dcurrie/ltcltk/        
- https://tset.de/ltcltk/index.html

# wxWidgets Bindings
- http://wxlua.sourceforge.net/     
- https://github.com/pkulchenko/wxlua