---
title: "Vdesk Un Lanzador para mplayer"
date: 2020-05-08T19:57:00-04:00
lastmod: 2020-05-08T19:57:00-04:00
draft: false
toc: false
tags: ["gtk","lgi","lua","lanzador"]
categories: ["Lua","GTK"]

toc: false
---

# Vdesk (Videos On Desktop)

Vdesk (Videos On Desktop) es un lanzador de videos para mplayer hecho en lua y gtk3 

- [develop](https://github.com/sodomon2/vdesk/archive/master.zip)

## Dependencias 

- [MPlayer](http://www.mplayerhq.hu/)
- [Lua-LGI](https://github.com/pavouk/lgi)
- [Lua](https://github.com/lua/lua)

### Ejecucion
Para ejecutar el lanzador es muy simple,solo tienes que seguir 3 pasos

- instalar lua y lgi
- descargar y extraer vdesk
- ejecutar lua `init.lua`

#### Enlaces 

- [Repositorio en github ](https://github.com/sodomon2/vdesk)
