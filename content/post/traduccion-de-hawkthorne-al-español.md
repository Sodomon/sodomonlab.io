---
title: "Traduccion de hawkthorne al español"
date: 2020-04-26T14:52:00-04:00
lastmod: 2020-04-26T14:52:00-04:00
draft: false
tags: ["hawkthorne","traduccion","lua"]
categories: ["Lua","Love2d"]

toc: false
---

# Traduccion de hawkthorne 
hawkthorne es un juego de plataformas en 2d encrito en lua utilizando el framework 
love2D ,el juego me parecio interesante pero me di cuenta que solo 
estaba en ingles y frances,por lo que decidi hacer mi propia traduccion al
español, en estos momentos la traduccion no a sido completada,pero pronto lo estara,
puedes descargar y revisar el progreso de la traduccion en el repositorio de github 

- [Love2D](http://love2d.org)
- [Descargar](https://github.com/sodomon2/hawkthorne-journey-Spanish/archive/master.zip)
- [Repositorio en github ](https://github.com/sodomon2/hawkthorne-journey-Spanish)

## Ejecucion
Para ejecutar el juego es simple,solo tienes que seguir 3 pasos

- descargar love2D 9.2
- descargar y extraer hawkthorne-journey-Spanish
- ejecutar love2d `/path/hawkthorne-journey-Spanish/`

## Enlaces 

- [love2D Windows](https://github.com/love2d/love/releases/download/0.9.2/love-0.9.2-win32.exe)
- [love2D Mac OS](https://github.com/love2d/love/releases/download/0.9.2/love-0.9.2-macosx-x64.zip)
- [Source love2D](https://github.com/love2d/love/releases/download/0.9.2/love-0.9.2-linux-src.tar.gz)
