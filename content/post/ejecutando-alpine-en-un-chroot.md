---
title: "ejecutando alpine en un chroot"
date: 2020-04-29T03:02:00-04:00
lastmod: 2021-01-06T18:16:20-04:00
draft: false
tags: ["alpine","chroot","guia"]
categories: ["Alpine Linux","Chroot","Guia"]

toc: false
---

# Como ejecutarlo
Bueno para empezar,lo primero que haremos sera descargar alpine miniroot,la
version que queramos puede ser de 32 BITS o de 64 BITS 

- [Descargar 32BITS](http://dl-cdn.alpinelinux.org/alpine/v3.11/releases/x86/alpine-minirootfs-3.11.6-x86.tar.gz)
- [Descargar 64BITS](http://dl-cdn.alpinelinux.org/alpine/v3.11/releases/x86_64/alpine-minirootfs-3.11.6-x86_64.tar.gz)

crearemos el directorio alpine-chroot y guardaremos la version correspodiente,extraemos en alpine-chroot 
y despues tendremos que ejecutar lo siguiente:

```
$ mount --bind /dev/ alpine-chroot/dev/
$ mount --bind /proc alpine-chroot/proc
$ mount --bind /sys alpine-chroot/sys
$ mount --bind /run alpine-chroot/run
$ mount --bind /dev/pts alpine-chroot/dev/pts
$ cp /etc/resolv.conf alpine-chroot/etc/
$ mkdir -p alpine-chroot /root
$ echo -e 'nameserver 8.8.8.8\nnameserver 2620:0:ccc::2' > alpine-chroot/etc/resolv.conf

$ mkdir -p alpine-chroot/etc/apk
$ echo "http://dl-cdn.alpinelinux.org/alpine/v3.11/main" > alpine-chroot/etc/apk/repositories
```

## Ejecucion
despues de hacer los todos los paso ejecutaremos el chroot con el siguiente comando
```
$ chroot alpine-chroot /bin/bash -l
```

despues de entrar a chroot añadiremos todos los procesos de la siguiente manera

```
rc-update add devfs sysinit
rc-update add dmesg sysinit
rc-update add mdev sysinit

rc-update add hwclock boot
rc-update add modules boot
rc-update add sysctl boot
rc-update add hostname boot
rc-update add bootmisc boot
rc-update add syslog boot

rc-update add mount-ro shutdown
rc-update add killprocs shutdown
rc-update add savecache shutdown
```

con eso ya tendriamos alpine instalado en un,chroot 
espero les haya gustado la guia. 😁

## Enlaces 

- [Guia oficial de alpine](https://wiki.alpinelinux.org/wiki/Alpine_Linux_in_a_chroot#Set_up_the_chroot)
- [Alpine Linux](https://alpinelinux.org)
